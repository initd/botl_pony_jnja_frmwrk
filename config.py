#-*- coding: utf-8 -*-

import os
### SERVER ###
APP_NAME = "test_server"
DEBUG_MODE = False
SERVER_LOG_NAME = "/var/log/bottle_sceleton.com/server-errors.log"

LISTEN_PORT = 10001
LISTEN_HOST = "0.0.0.0"
HOST = "test_sceleton.com"

### DB ###
DB_TYPE = "postgresql"  # postgresql | mysql | sqlite
DB_HOST = "localhost"
DB_PORT = 5432
DB_USER = "test_user"
DB_PASS = "test_password"
DB_NAME = "test_db"      # path to database file if SQLite
DB_DEBUG = False

### REDIS ###
REDIS_DB = 0
REDIS_HOST = "127.0.0.1"
REDIS_PORT = 6379

### SESSIONS ###
SESSION_AUTO_SAVE = True
SESSION_AUTO_START = True
SESSION_KEY = "PHPSESSID"
SESSION_SALT = "tetststettedfsdferre"
SESSION_TIME = 86400
SESSION_STORAGE = "redis"
SESSION_PREFIX = "test_srvc_sess_"
SESSION_REVERS_PREFIX = "test_srvc_reverse_"
SESSION_UNIQUE = True

### SECURE ###
TOKEN_SALT = "tettetstetsteststetstestettset"

### BUSINESS LOGIC ###

BL_PAGESIZE = 1000
BL_ITEMS_PER_PAGE = 25
BL_ADMIN_USERS_ITEMS_PER_PAGE = 50
BL_ADMIN_ID = 1

### QUEUES ###
HOT_QUEUE_NAME = "test_queue"
HOT_MESSAGES_QUEUE_NAME = "test_msg_queue"

### EMAIL ###
EMAIL_USER_NAME = 'user@example.com'
EMAIL_USER_PSW = 'some_password'
EMAIL_SMTP_SERVER = 'smtp.gmail.com'
EMAIL_SMTP_PORT = 587
EMAIL_CODEPAGE = 'utf-8'
EMAIL_NAME_FROM = u'Some N service'

########## SMS ##############
# Константы для настройки библиотеки SMSC
SMSC_LOGIN = "smsc_login"            # логин клиента
SMSC_PASSWORD = "smsc_password"    # пароль или MD5-хеш пароля в нижнем регистре
SMSC_POST = False       # использовать метод POST
SMSC_HTTPS = False      # использовать HTTPS протокол
SMSC_CHARSET = "utf-8"  # кодировка сообщения (windows-1251 или koi8-r), по умолчанию используется utf-8
SMSC_DEBUG = True       # флаг отладки
SMSC_SENDER = "SomeServiceN"
##############################


STATIC_URL = "/static"

try:
    from config_local import *
except ImportError:
    pass

try:
    from config_strings import *
except ImportError:
    pass


DIR_ROOT = os.path.dirname(os.path.realpath(__file__))
DIR_UPLOADS = "/tmp"
