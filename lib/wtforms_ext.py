# -*- coding: utf-8 -*-
"""
"""
__author__ = "Mednikov Yuriy <mednikov.yuriy@gmail.com>"

from lib.time_helper import date_to_dt
from lib.time_helper import dt_to_unixtime
from lib.time_helper import unixtime_to_date

from wtforms.fields import Field
from wtforms.widgets import TextInput

filter_strip = lambda x: x.strip() if x else None


class TimestampField(Field):
    widget = TextInput()

    def __init__(self, label=None, validators=None, format='%Y-%m-%d %H:%M:%S',
                 **kwargs):
        super(TimestampField, self).__init__(label, validators, **kwargs)
        self.format = format

    def pre_validate(self, value):
        dt = date_to_dt(value.data.get("valid_thru"), date_fmt=self.format)
        self.data = dt_to_unixtime(dt)

    def _value(self):

        if self.raw_data:
            return ' '.join(self.raw_data)
        if self.data:
            return unixtime_to_date(self.data, datetime_fmt=self.format)
        return ""
