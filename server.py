#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
import config
import logging
import traceback


from bottle import run
from bottle import hook
from bottle import route
from bottle import request
from bottle import response
from bottle import default_app
from bottle import HTTPError
from bottle import HTTPResponse
from bottle import static_file

from pony.orm import db_session

from json import dumps as json_encode

from lib.time_helper import unixtime_curr_utc
from lib.time_helper import unixmtime_curr_utc

from application.session import Session


logging.basicConfig(filename=config.SERVER_LOG_NAME,
                    format=logging.BASIC_FORMAT)


@hook("before_request")
def before_request():
    request.time = time.time()
    request.time10 = unixtime_curr_utc()
    request.time13 = unixmtime_curr_utc()
    request.timezone = "GMT"
    request.logging = logging

    request.session = Session(
        request=request,
        response=response,
        auto_save=config.SESSION_AUTO_SAVE,
        auto_start=config.SESSION_AUTO_START,
        session_key=config.SESSION_KEY,
        session_salt=config.SESSION_SALT,
        session_time=config.SESSION_TIME,
        storage_type=config.SESSION_STORAGE,
        storage_settings={
            "db": config.REDIS_DB,
            "host": config.REDIS_HOST,
            "port": config.REDIS_PORT,
            "prefix": config.SESSION_PREFIX,
            "reverse_pref": config.SESSION_REVERS_PREFIX,
            "is_user_unique": config.SESSION_UNIQUE,
        },
    )


@hook("after_request")
def after_request():
    time_exec = (unixmtime_curr_utc() - request.time13) / 1000.
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["X-Time-Exec"] = "%.05f" % time_exec
    request.session.process_autosave()


def errors_handler(callback):
    def print_traceback():
        """ This method extract full callstack and return string representation
            :return string str: formatted callstack
        """
        exc = sys.exc_info()[0]
        stack = traceback.extract_stack()[:-1]
        if not exc is None:
            del stack[-1]
        trc = "Traceback (most recent call last):\n"
        stackstr = trc + "".join(traceback.format_list(stack))
        if not exc is None:
            stackstr += "  " + traceback.format_exc().lstrip(trc)
        print stackstr

    def wrapper(*args, **kwargs):
        try:
            with db_session:
                return callback(*args, **kwargs)
        except HTTPResponse, e:
            raise
        except HTTPError, e:
            print_traceback()
            return json_encode({"status": "error", "message": e.body})
        except Exception, e:
            raise
            print_traceback()
            return json_encode({"status": "error", "message": str(e)})
    return wrapper

"""
This is recursive import, that importing all routes from applications.
For understanding see applications\router\__init__.py.
"""
from application.router import *
app = default_app()
app.install(errors_handler)


@route('/static/<path:path>')
def static_server(path):
    return static_file(path, root="./static")

if __name__ == "__main__":
    run(app=app, debug=config.DEBUG_MODE, reloader=True,
        host=config.LISTEN_HOST, port=config.LISTEN_PORT)
else:
    if not config.DEBUG_MODE:
        app.catchall = False
