# -*- coding: utf-8 -*-
#!/usr/bin/env python
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'


from application.workers.notificator import queue_sender
import datetime as dt
datetime_fmt = "%Y-%m-%d %H:%M:%S"


def main():
    try:
        queue_sender.main()
    except Exception as e:
        # TODO: log exception
        print "Re-run now!"
        print dt.datetime.now().strftime(datetime_fmt), e
        main()  # re-run


if __name__ == '__main__':
    main()