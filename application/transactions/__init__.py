# -*- coding: utf-8 -*-
#!/usr/bin/env python
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

from time import time

from db import CommitException
from db.mixins import TransactionMixin
#from db.models import Transaction
# see underline "dummy" class Transaction, not reallife code!!!


class Transaction(object, TransactionMixin):
    def __init__(self):
        pass

from application.config import config_accessor


class TransactionProvider(object):
    trn_types = ("balance", "other_property")
    """
    Transaction check pattern:
        trn_provdr = TransactionProvider(curr_user)
        result = trn_provdr.balance_change_start(delta_balance)
        if result["amount"] != delta_balance:
            request.session.flash(u"Значение баланса пользователя не изменено"
                                  u":%s" % result["msg"],
                                  "error")
            return redirect("/admin/users/%d" % curr_user.id)
        request.session.set_user_info_invalid_actuality()
        request.session.flash(u"Значение баланса пользователя изменено "
                              u"на %d" % result["amount"], "success")
    """
    def __init__(self, user):
        self.user = user

    def get_changing_property(self, trn_type):
        if trn_type not in self.trn_types:
            raise Exception(msg="Invalid transaction type")
        if trn_type == "balance":
            return self.user.balance
        elif trn_type == "other_property":
            return self.user.other_property_limit

    def register(self, trn_type, comment=None, RUB=0, from_user=None,
                 external_transaction=None, promo_transaction=None):
        new_trn = Transaction(
            start_time=int(time()),
            RUB=RUB,
            trn_type=trn_type,
            comment=comment,
            to_user=self.user,
            from_user=from_user,
            external_transaction=external_transaction,
            promo_transaction=promo_transaction
        )
        new_trn.save()
        return new_trn

    def commit(self, transaction):
        if transaction.timecommited:
            return False
        self.user.balance += transaction.cur
        transaction.commit_time = int(time())
        transaction.save()
        return True

    def check_ability(self, prop, delta):
        return prop + delta >= 0

    def start_transaction(self, changing_property, delta):
        result = dict(msg=u"Error",
                      amount=None)
        if self.check_ability(changing_property, delta):
            return result
        try:
            if self.register():
                result = dict(msg=u"Success",
                              amount=delta)
        except CommitException:
            pass
        return result

    def admin_balance_change_start(self, delta, from_user=None):
        trn_type = Transaction.TRN_TYPES[u"AdminPay"]
        comment = u"Бонус от администратора"
        return self.balance_change_start(delta, trn_type, comment, from_user)

    def system_balance_change_start(self, delta):
        trn_type = Transaction.TRN_TYPES[u"SystemPay"]
        comment = u"Бонус при регистрации"
        return self.balance_change_start(delta, trn_type, comment)

    def balance_change_start(self, delta, trn_type, comment=None,
                             from_user=None):
        if not self.check_ability(self.user.balance, delta):
            return dict(msg=u"Insufficient funds",
                        amount=None)
        curr_trn = self.register(trn_type=trn_type, comment=comment, cur=delta,
                                 from_user=from_user)
        if not self.commit(curr_trn):
            return dict(msg=u"Balance change transaction commit error",
                        amount=None)
        return dict(msg=u"Success change balance",
                    amount=delta)

    def consume_referrer_start(self, from_user):
        delta = config_accessor.get_consumation_value()
        trn_type = Transaction.TRN_TYPES[u"SystemDmnLimitChange"]
        comment = u"Вознаграждение за привлечение пользователя %s" % from_user.email
        from_user.is_ref_consumed = True
        return self.limit_change_start(delta, trn_type, comment, from_user)


def main():
    pass


if __name__ == '__main__':
    main()