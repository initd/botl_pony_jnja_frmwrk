#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

import config
from db import db_session
from db.models import Config


class ConfigAccessor(object):
    def __init__(self):
        self.__dict__ = config.__dict__
        self.prop_db_inst_correlation = dict(
            default_user_balance="BL_DEFAULT_USER_BALANCE"
        )
        self.update_configs_from_db()

    def update_configs_from_db(self):
        with db_session:
            configs_from_db = Config.get_items()
        for config_prop in configs_from_db:
            self.__dict__[self.prop_db_inst_correlation[config_prop.key]] = \
                config_prop.value

    def get_items_per_page(self):
        return self.BL_ITEMS_PER_PAGE

    def get_admin_users_items_per_page(self):
        return self.BL_ADMIN_USERS_ITEMS_PER_PAGE

    def get_upload_dir(self):
        return self.DIR_UPLOADS

    def get_global_admin_id(self):
        return self.BL_ADMIN_ID

    def get_host(self):
        return self.HOST


config_accessor = ConfigAccessor()


def main():
    with db_session:
        pass


if __name__ == '__main__':
    main()