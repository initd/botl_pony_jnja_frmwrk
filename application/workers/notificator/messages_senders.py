#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

import config

from lib.smsc import smsc
from lib.gmail_sender import MailSender

from application.workers.hotqueue.conn import messages_queue


class SMS(object):
    def __init__(self):
        self.sender = smsc
        self.sender_name = config.SMSC_SENDER

    def send_sms(self, num, text, sender=None):
        if sender:
            self.sender.send_sms(num, text, sender=sender)
            return
        smsc.send_sms(num, text, sender=self.sender_name)

    def send_activation_sms(self, phone, token):
        self.send_sms(phone, config.SMS_ACTIVATION_TEXT % token)

    def send_dup_activation_sms(self, phone, token):
        self.send_sms(phone, config.SMS_DUP_ACTIVATION_TEXT % token)


class Email(object):
    def __init__(self):
        self.sender = MailSender(
            config.EMAIL_SMTP_SERVER,
            config.EMAIL_SMTP_PORT,
            config.EMAIL_USER_NAME,
            config.EMAIL_USER_PSW,
            config.EMAIL_NAME_FROM,
            config.EMAIL_CODEPAGE
        )

    def send_email(self, to_user, subject, text_part, html_part=None):
        self.sender.send_mail(to_user, subject, text_part, html_part)

    def send_register_email(self, to_user, data):
        self.send_email(to_user,
                        config.EMAIL_REGISTER_SUBJ,
                        config.EMAIL_REGISTER_TEXT % data,
                        config.EMAIL_REGISTER_HTML % data)

    def send_password_remind_email(self, to_user, data):
        self.send_email(to_user,
                        config.EMAIL_PSW_REMIND_SUBJ,
                        config.EMAIL_PSW_REMIND_TEXT % data,
                        config.EMAIL_PSW_REMIND_HTML % data)

    def send_password_change_email(self, to_user, data):
        self.send_email(to_user,
                        config.EMAIL_PSW_CHANGE_SUBJ,
                        config.EMAIL_PSW_CHANGE_TEXT % data,
                        config.EMAIL_PSW_CHANGE_HTML % data)

    def send_login_change_email(self, to_user, data):
        self.send_email(to_user,
                        config.EMAIL_LOGIN_CHANGE_SUBJ,
                        config.EMAIL_LOGIN_CHANGE_TEXT % data,
                        config.EMAIL_LOGIN_CHANGE_HTML % data)

    def send_activation_email(self, to_user, data):
        self.send_email(to_user,
                        config.EMAIL_DUP_ACTIVATION_SUBJ,
                        config.EMAIL_DUP_ACTIVATION_TEXT % data,
                        config.EMAIL_DUP_ACTIVATION_HTML % data)

    def send_insufficient_funds_email(self, to_user, data):
        self.send_email(to_user,
                        config.EMAIL_INSUFFICIENT_FUNDS_SUBJ,
                        config.EMAIL_INSUFFICIENT_FUNDS_TEXT % data,
                        config.EMAIL_INSUFFICIENT_FUNDS_HTML % data)


class MessagesQueuer(object):
    def __init__(self, queue_connect):
        self.queue = queue_connect

    def queue_add(self, message_data):
        self.queue.put(message_data)


class SMSQueuer(MessagesQueuer, SMS):
    def send_sms(self, num, text, sender=None):
        sms_data = dict(type="sms", num=num, text=text, sender=sender)
        self.queue_add(sms_data)


class EmailQueuer(MessagesQueuer, Email):
    def send_email(self, to_user, subject, text_part, html_part=None):
        email_data = dict(type="email",
                          to_user=to_user,
                          subject=subject,
                          text_part=text_part,
                          html_part=html_part)
        self.queue_add(email_data)


sms = SMS()
email = Email()
sms_q = SMSQueuer(messages_queue)
email_q = EmailQueuer(messages_queue)


def test():
    default_sms_sender = SMS()
    default_email_sender = Email()

    default_email_sender.send_email(config.EMAIL_USER_NAME,
                                    config.EMAIL_TEST_SUBJ,
                                    config.EMAIL_TEST_TEXT,
                                    config.EMAIL_TEST_HTML)

    #default_sms_sender.send_sms("7987654321", "test")
    print default_sms_sender.sender.get_balance()

if __name__ == '__main__':
    test()
