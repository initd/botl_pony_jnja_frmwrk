#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

from application.workers.notificator.messages_senders import sms, email
from application.workers.hotqueue.conn import messages_queue


def main():
    for message in messages_queue.consume():
        msg_type = message["type"]
        del message["type"]
        if msg_type == "sms":
            sms.send_sms(**message)
        elif msg_type == "email":
            email.send_email(**message)


if __name__ == '__main__':
    main()