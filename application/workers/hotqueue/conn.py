#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

import config
from hotqueue import HotQueue

messages_queue = HotQueue(config.HOT_MESSAGES_QUEUE_NAME,
                          host=config.REDIS_HOST,
                          port=config.REDIS_PORT,
                          db=config.REDIS_DB)