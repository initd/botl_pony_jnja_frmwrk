#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

import datetime as dt

from bottle import route, request
from bottle import jinja2_view as view

from lib.auth import require_auth
from lib.response import response_ok
from lib.time_helper import dt_to_unixtime
from db.models import User


def days_to(days):
        return dt_to_unixtime(dt.datetime.now() + dt.timedelta(days=days))


@route("/")
@view("views/index.html")
def route_index():
    total_users = User.get_items_count()
    data = dict(total_users=total_users)
    return response_ok({"data": data})


@route("/dashboard")
@require_auth
@view("views/dashboard/index.html")
def route_dashboard():
    counter = {
        "all": 0,
        ".com": 0,
        ".ru": 0,
        ".net": 0,
        "60d": 0,
        "30d": 0,
        "2week": 0,
        "expired": 0,

    }
    request.session.data["referrer"] = request.path
    return response_ok({"counter": counter})


@route("/dashboard/help")
@view("views/dashboard/help.html")
def route_help():
    return response_ok()
