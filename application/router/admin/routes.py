#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

from bottle import jinja2_view as view
from bottle import route, request, redirect

from lib.response import response_ok
from lib.auth import require_auth, require_admin

from application.config import config_accessor

from db.models import User, Config


@route("/admin")
@require_auth
@require_admin
@view("views/admin/index.html")
def route_admin_dashboard():
    total_users = User.get_items_count()
    expr = (u.parent for u in User)
    total_referrals = User.get_items_count(expr=expr)
    data = dict(total_users=total_users,
                total_referrals=total_referrals)
    return response_ok({"data": data})


@route("/admin/settings")
@require_auth
@require_admin
@view("views/admin/global_settings.html")
def route_admin_global_config():
    configs = Config.get_items()
    form_data = dict()
    for config in configs:
        form_data[config.key] = config.value
    return response_ok({"settings_form": None,
                        "promo_form": None})


@route("/admin/settings/save", method="POST")
@require_auth
@require_admin
@view("views/admin/global_settings.html")
def route_admin_global_config():
    settings_form = None
    if not settings_form.validate():
        promo_form = None
        return response_ok({"settings_form": settings_form,
                            "promo_form": promo_form})

    configs = Config.get_items()
    for config in configs:
        if not config.value == settings_form.data.get(config.key):
            config.value = settings_form.data.get(config.key)
            config.save()

    config_accessor.update_configs_from_db()
    request.session.flash(u"Настройки успешно сохранены", "success")
    return redirect("/admin/settings")
