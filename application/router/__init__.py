# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

#from config import *
from application.router.users.routes import *
from application.router.admin.routes import *
from application.router.dashboard.routes import *