# -*- coding: utf-8 -*-
"""
"""
__author__ = ["Mednikov Yuriy <mednikov.yuriy@gmail.com>",
              'Rodion Promyshlennikov <rodion.prm@gmail.com> '
              '{matrix or initd}']


import re
from bottle import request

from wtforms import Form
from wtforms import StringField
from wtforms import IntegerField
from wtforms import BooleanField
from wtforms import PasswordField
from wtforms import validators
from wtforms import ValidationError

from db.models import User

from lib.wtforms_ext import filter_strip


class UserRegisterForm(Form):
    id_referrer = StringField(
        u"Идентификатор рефферала"
    )

    email = StringField(
        u"Email",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Email(message=u"Невалидный email"),
         validators.Length(min=6, message=u"Слишком короткий email")],
        filters=[filter_strip]
    )

    phone = StringField(
        u"Телефон",
        filters=[filter_strip]
    )

    password = PasswordField(
        u"Пароль",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(min=5)],
        filters=[filter_strip]
    )

    news_subscribed = BooleanField(
        u"Рассылка новостей системы",

    )

    def validate_phone(self, field):
        if not field.data:
            return True
        if len(field.data) > 12:
            raise ValidationError(
                u"Длина телефона не должна превышать 12 знаков"
            )
        regex = re.compile("\+[7][9]\d{9}")
        if not regex.match(field.data or ''):
                raise ValidationError(u"Невалидный телефон, "
                                      u"формат: +7987654321")

        if request.session.data.get("id_user") and \
                User[request.session.data.get("id_user")].phone == field.data:
            return True
        if User.get(phone=field.data):
            raise ValidationError(
                u"Пользователь с таким телефоном уже существует")

    def validate_email(self, field):
        if request.session.data.get("email") == field.data.lower():
            return True
        if User.get(email=field.data.lower()):
            raise ValidationError(
                u"Пользователь с такой почтой уже существует")

    def validate_id_referrer(self, field):
        if not field.data:
            return True
        try:
            int(field.data)
        except ValueError:
            field.data = "0"
            return True


class UserLoginForm(Form):
    email = StringField(
        u"Email",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Email(message=u"Невалидный email"),
         validators.Length(min=6, message=u"Слишком короткий email")],
        filters=[filter_strip]
    )

    password = PasswordField(
        u"Пароль",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(min=5)],
        filters=[filter_strip]
    )


class UserPasswordRequestForm(Form):
    email = StringField(
        u"Email",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Email(message=u"Невалидный email"),
         validators.Length(min=6, message=u"Слишком короткий email")],
        filters=[filter_strip]
    )

    def validate_email(self, field):
        if not User.get(email=field.data.lower()):
            raise ValidationError(
                u"Пользователя с такой почтой не существует")


class UserVerifyForm(Form):
    token = StringField(
        u"Код верификации",
        [validators.DataRequired(message=u"Заполните поле")],
        filters=[filter_strip]
    )


class PhoneVerifyForm(UserVerifyForm):
    pass


class NewPasswordForm(UserVerifyForm):
    new_password = PasswordField(
        u"Новый пароль",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(min=5)]
    )


class UserPasswordChangeForm(Form):
    old_password = PasswordField(
        u"Старый пароль",
        [validators.DataRequired(message=u"Заполните поле")]
    )
    new_password = PasswordField(
        u"Новый пароль",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(min=5),
         validators.EqualTo("confirm", message=u"Пароли должны совпадать")]
    )

    confirm = PasswordField(
        u"Повторите пароль"
    )


class AdminUserChangeForm(Form):
    password = PasswordField(
        u"Новый пароль",
        filters=[filter_strip]
    )

    def validate_password(self, field):
        if field.data and len(field.data) < 5:
            raise ValidationError(
                u"Длина пароля должна составлять минимум 5 символов")