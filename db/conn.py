# -*- coding: utf-8 -*-
"""
based on version by:
"Mednikov Yuriy <mednikov.yuriy@gmail.com>"
"""

__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

import config

from pony.orm import sql_debug
from pony.orm import Database


db = None

if not db:
    if config.DB_TYPE not in ["sqlite", "postgresql"]:
        print "Unknown database type"
        exit(-1)

    if config.DB_TYPE == "sqlite":
        db = Database("sqlite", config.DB_NAME, create_db=True)

    elif config.DB_TYPE == "postgresql":
        db = Database("postgres", host=config.DB_HOST, port=config.DB_PORT,
                      user=config.DB_USER, password=config.DB_PASS,
                      database=config.DB_NAME)
    sql_debug(config.DB_DEBUG)