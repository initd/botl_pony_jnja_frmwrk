#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'


from pony.orm import Set
from pony.orm import Optional
from pony.orm import Required
from pony.orm import PrimaryKey

from db.conn import db
from db.mixins import SiteItem
from db.mixins import TreeMixin

from lib.time_helper import unixtime_to_date

default_date_fmt = "%d-%m-%Y"
default_datetime_fmt = "%d-%m-%Y %H:%M:%S"


class User(SiteItem, TreeMixin, db.Entity):
    _table_ = "users"
    id = PrimaryKey(int, auto=True)
    email = Required(unicode, 255, unique=True)
    password = Required(unicode, 64)
    phone = Optional(unicode, 15, unique=True, nullable=True)
    balance = Required(int, default=0)
    tree_path = Required(unicode, 256)
    time_created = Required(int)
    time_activated = Optional(int, default=0)
    time_phone_activated = Optional(int, default=0)
    time_locked = Optional(int, default=0)
    time_deleted = Optional(int, default=0)
    news_subscribed = Required(bool)
    parent = Optional("User", reverse="children")
    children = Set("User", reverse="parent")

    @property
    def timecreated(self):
        return unixtime_to_date(self.time_created, default_date_fmt)

    @property
    def ref_count(self):
        return sum(1 for u in self.children)


class Config(SiteItem, db.Entity):
    _table_ = "configs"
    id = PrimaryKey(int, auto=True)
    key = Required(unicode, 253, unique=True)
    value = Required(int)

db.generate_mapping(create_tables=True)
