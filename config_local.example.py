#-*- coding: utf-8 -*-

APP_NAME = "test_service"
DEBUG_MODE = True

DB_TYPE = "postgresql"  # postgresql | mysql | sqlite
DB_HOST = "localhost"
DB_PORT = 5432
DB_USER = "db_user"
DB_PASS = "db_pass"
DB_NAME = "db_name"      # path to database file if SQLite
DB_DEBUG = False         # output sql queries

REDIS_DB = 0
REDIS_HOST = "127.0.0.1"
REDIS_PORT = 6379
REDIS_PREFIX = "test_srvc_sess_"

SESSION_AUTO_SAVE = True
SESSION_AUTO_START = True
SESSION_KEY = "PHPSESSID"
SESSION_SALT = "tetaetstetsstestsetstseteste"
SESSION_TIME = 86400
SESSION_STORAGE = "redis"
