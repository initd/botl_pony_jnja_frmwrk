#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dev.db.mocks import configs

__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'

import sys

sys.path.append("../../")

from pony.orm import db_session

from db.models import Config


with db_session:
    for config in configs:
        new_config = Config(**config)
        new_config.save()
