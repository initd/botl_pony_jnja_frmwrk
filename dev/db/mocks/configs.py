#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'


configs = [

    dict(id=1,
         key=u"opt1",
         value=100),

    dict(id=2,
         key=u"opt2",
         value=50),
]
