#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'


import bcrypt
from time import time

T = int(time())


def gen_psw_hash(password):
    return bcrypt.hashpw(password, bcrypt.gensalt())

users = [
        dict(id=1,
             email="rodion.prm@gmail.com",
             password=gen_psw_hash("password"),
             phone="+7987654321",
             balance=5000,
             tree_path="/1/",
             time_created=T,
             time_activated=T,
             time_phone_activated=T,
             news_subscribed=True,
             parent=None),]

"""        dict(id=2,
             email="user02@user.com",
             password=gen_psw_hash("user02"),
             phone="+79876543212",
             tree_path="/1/2/",
             time_created=T,
             time_activated=0,
             time_phone_activated=T,
             news_subscribed=False,
             parent=1),

        dict(id=3,
             email="user03@user.com",
             password=gen_psw_hash("user03"),
             phone="+79876543213",
             tree_path="/1/3/",
             time_created=T,
             time_activated=T,
             time_phone_activated=T,
             news_subscribed=True,
             parent=1),

        dict(id=4,
             email="user04@user.com",
             password=gen_psw_hash("user04"),
             phone="+79876543214",
             tree_path="/1/4/",
             time_created=T,
             time_activated=T,
             time_phone_activated=0,
             news_subscribed=False,
             parent=1),

        dict(id=5,
             email="user05@user.com",
             password=gen_psw_hash("user05"),
             phone="+79876543215",
             tree_path="/1/5/",
             time_created=T,
             time_activated=0,
             time_phone_activated=0,
             news_subscribed=True,
             parent=1),

        dict(id=6,
             email="user06@user.com",
             password=gen_psw_hash("user06"),
             phone="+79876543216",
             tree_path="/1/6/",
             time_created=T,
             time_activated=0,
             time_phone_activated=0,
             news_subscribed=False,
             parent=1),
]"""