#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Rodion Promyshlennikov <rodion.prm@gmail.com> {matrix or initd}'


import sys

sys.path.append("../../")

import bcrypt

from pony.orm import db_session

from dev.db.mocks.users import users
from db.models import User


psw_hash = bcrypt.hashpw("password", bcrypt.gensalt())
with db_session:
    for user in users:
        if user["parent"]:
            user["parent"] = User[user["parent"]]

        else:
            del user["parent"]

        new_user = User(**user)
        new_user.save()
