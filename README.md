# Framework Base#

### This sceleton sources help in developing any web project ###

* Set of: BottlePy + PonyOrm + Jinja2 + Wtforms + Bootstrap(jquery, sbadmin2)
* 1.0
* This is NOT completed Webapp, just base for it.

### Setup ###

* Fork, not clone.
* Edit config
* pip install -r requirements.txt

### Run ###

* Run python server.py


### Dependencies for development layer ###
* WTForms==2.0.1 - forms completely processing
* bottle==0.12.7 - main framework
* hotqueue==0.2.7 - if you want use something async (messaging, notify, other blocking operations)
* pony==0.5.3 - ORM
* py-bcrypt==0.4 - for passwords secure crypt hashing
* pytz==2014.4 - for datatime working
* redis==2.10.3 - for hotqueue and session storage
* Jinja2==2.7.3 - templating language

### Deployment instructions ###

* git pull on production master
* see ./dev/ for config examples (nginx, supervisor)
* edit config.py for other needed options
* setup needed external dependencies (postgreSQL, nginx, supervisor)

### Contribution guidelines ###

* We all adults here...

### Who do I talk to? ###

* Rodion <rodion.prm@gmail.com>