/*
* @Author:
* @Date:   2014-09-11 15:28:28
* @Last Modified by:   user
* @Last Modified time: 2014-09-25 16:44:52
*/

$(document).ready(function(){
    $(".delete-device").click(function(){
        var id = $(this).data("id");
        alertify.confirm("Do you really wanna delete device?", function(e){
            if(e) {
                $('<form method="POST" action="/devices/delete"><input type="text" name="id" value="'+id+'"></form>').appendTo('body').submit();
            }
        });
    });

    $("#time_started").datetimepicker({
        language: "ru",
        useSeconds: false,
        format: "YYYY-MM-DD HH:mm",
    });

    $("select[multiple]").multiselect({
        enableFiltering: true,
        includeSelectAllOption: true,
        filterBehavior: "text",
        enableCaseInsensitiveFiltering: true,
    });

    $("#unit-types-selector").bootstrapTable();
    $("#unit-types-form").submit(function() {
        $table = $("#device-types-selector").bootstrapTable();
        $.each($table.bootstrapTable("getSelections"), function(index, row){
            $("#unit-types-form").append('<input type="hidden" value="' + row.id + '" name="device_types" />');
        });
    });

    $("#device-types-selector").bootstrapTable();
    $("#device-types-form").submit(function() {
        $table = $("#injects-selector").bootstrapTable();
        $.each($table.bootstrapTable("getSelections"), function(index, row){
            $("#task-form").append('<input type="hidden" value="' + row.element + '" name="unit_types" />');
        });
    });
});