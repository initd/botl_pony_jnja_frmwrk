/*
* @Author:
* @Date:   2014-09-27 15:17:10
* @Last Modified by:   user
* @Last Modified time: 2014-09-27 20:00:01
*/

function strToIntSorter(a, b) {
        a = +a;
        b = +b;
        if (a > b) return 1;
        if (a < b) return -1;
        return 0;
    };

$(document).ready(function(){

    $("#valid_thru").datetimepicker({
        language: "ru",
        useSeconds: false,
        format: "YYYY-MM-DD HH:mm",
    });

    $("select[multiple]").multiselect({
        numberDisplayed: 10,
        includeSelectAllOption: true,
        nonSelectedText: "Не отправлять",
    });


    $("#payments_table").bootstrapTable();
    $("#admin_users_table").bootstrapTable();

    /** Handle "set mode" buttons */
    $(".elements_set_mode").click(function(){
        var ud_ids = [];
        var set_mode = $(this).data("set-mode");

        $.each($("#elements_table").bootstrapTable("getSelections"), function(index, row){
            ud_ids.push(row.id);
        });

        if(!ud_ids.length) {
            alertify.alert("Для смены типа мониторинга выберите, пожалуйста, элемент.", function(){
                alertify.message('OK');
            });
            return false;
        }

        /** Create form */
        $('<form id="form_set_mode" method="POST" action="/elements/set_mode"></form>').appendTo('body');
        form = $("#form_set_mode");

        /** Declare action */
        form.append('<input type="hidden" name="set_mode" value="' + set_mode + '" />');

        /** Append id`s into form */
        $.each(ud_ids, function(index, value){
            form.append('<input type="hidden" name="elements" value="' + value + '" />');
        })

        form.submit();
    });

    /** Handle "delete" button */
    $(".elements_delete").click(function(){
        var ud_ids = [];

        $.each($("#elements_table").bootstrapTable("getSelections"), function(index, row){
            ud_ids.push(row.id);
        });

        if(!ud_ids.length) {
            alertify.alert("Для удаления выберите, пожалуйста, elements.", function(){
                alertify.message('OK');
            });
            return false;
        }
        alertify.confirm("Вы уверены, что хотите удалить выбранные elements?", function(e){
            if(e) {
                $('<form id="form_delete" method="POST" action="/elements/delete"></form>').appendTo('body');
                form = $("#form_delete");

                /** Append id`s into form */
                $.each(ud_ids, function(index, value){
                    form.append('<input type="hidden" name="elements" value="' + value + '" />');
                })

                form.submit();
            }
        });
    });


    /** Handle "update info" button */
    $(".elements_update").click(function(){
        var ud_ids = [];

        $.each($("#elements_table").bootstrapTable("getSelections"), function(index, row){
            ud_ids.push(row.id);
        });

        if(!ud_ids.length) {
            alertify.alert("Для обновления информации выберите, пожалуйста, elements.", function(){
                alertify.message('OK');
            });
            return false;
        }
 
  
        $('<form id="form_update_info" method="POST" action="/elements/update_info"></form>').appendTo('body');
        form = $("#form_update_info");

        /** Append id`s into form */
        $.each(ud_ids, function(index, value){
            form.append('<input type="hidden" name="elements" value="' + value + '" />');
        })

        form.submit();
    });

    $("#add_group_btn").click(function(event) {
        var curr_group = $("#elements_table").bootstrapTable("getSelections")[0].alias.trim();
        console.log(curr_group);
        console.log(curr_group.length);
        $('#group_input').val(curr_group);
    });

    $(".elements_add_group").click(function(){
        var new_group = $('#group_input').val();
        var ud_ids = [];

        $.each($("#elements_table").bootstrapTable("getSelections"), function(index, row){
            ud_ids.push(row.id);
        });

        if(!ud_ids.length) {
            alertify.alert("Для добавления в группу выберите, пожалуйста, elements.", function(){
                alertify.message('OK');
            });
            return false;
        }
 
  
        $('<form id="form_add_group" method="POST" action="/elements/add_group"></form>').appendTo('body');
        form = $("#form_add_group");

        form.append('<input type="hidden" name="group" value="' + new_group + '" />');

        /** Append id`s into form */
        $.each(ud_ids, function(index, value){
            form.append('<input type="hidden" name="elements" value="' + value + '" />');
        })

        form.submit();
    });

    $(".elements_delete_group").click(function(){
        var ud_ids = [];

        $.each($("#elements_table").bootstrapTable("getSelections"), function(index, row){
            ud_ids.push(row.id);
        });

        if(!ud_ids.length) {
            alertify.alert("Для добавления в группу выберите, пожалуйста, elements.", function(){
                alertify.message('OK');
            });
            return false;
        }
 
  
        $('<form id="form_add_group" method="POST" action="/elements/add_group"></form>').appendTo('body');
        form = $("#form_add_group");

        form.append('<input type="hidden" name="group" value="" />');

        /** Append id`s into form */
        $.each(ud_ids, function(index, value){
            form.append('<input type="hidden" name="elements" value="' + value + '" />');
        })

        form.submit();
    });


    /** Handle "Find user" buttons */
    $(".user_search").click(function(event){
        var value = $(".user-ident-search-form").val();
        if (!value) {
            event.preventDefault();
            alertify.alert("Должен быть указан email или ID пользователя.");
        } else {
            $('<form id="form_search_user" method="GET" action="/admin/users"></form>').appendTo('body');
                form = $("#form_search_user");
                form.append('<input type="hidden" name="ident" value="' + value + '" />');
                form.submit();
        }       

    });

    var payRub = $('#pay-rub').val();
    payRub = +payRub;
    payRub = payRub.toFixed(3).slice(0, -1);
    $('#pay-rub').val(payRub);

    $('#pay-pt').bind('textchange', function(event, previousText) {
        if (this.value.match(/[^\d]/g)) {
            this.value = this.value.replace(/[^\d]/g, '');
        }
        var k = $('#currency').val()
        var ptValue = $(this).val();
        var pubValue = (ptValue * k).toFixed(3).slice(0, -1);
        $('#pay-rub').val(pubValue);
    });

    $("#payment-btn").click(function(event) {
        var ptValue = $('#pay').val();
        var minPayIn = $('#min_payin').val();
        if (ptValue < minPayIn) {
            $("#notifycator-pay").append("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Минимальное количество N elements12</div>");
            event.preventDefault();
        }
    });

    $('#extend-limit').bind('textchange', function(event, previousText) {
        if (this.value.match(/[^\d]/g)) {
            this.value = this.value.replace(/[^\d]/g, '');
        }
        var price = $('#elements_price').val()
        var extendAmountValue = $(this).val();
        var Value = (extendAmountValue * price);
        $('#purchase').val(ptValue);
    });

    $("#purchase-btn").click(function(event) {
        var Value = $('#purchase').val();
        var balance = $('#balance').val();
        if (+Value < 1) {
            $("#notifycator-purchase").append("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Количество должно быть указано и быть больше нуля.</div>");
            event.preventDefault();
        }
        if (+ptValue > +balance) {
            $("#notifycator-purchase").append("<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Стоимость приобретаемых elements превышает Ваш баланс.</div>");
            event.preventDefault();
        }
    });

});